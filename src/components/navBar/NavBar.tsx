import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Container from "@mui/material/Container";
import { useRouter } from "next/router";
import NavBarALink from './NavBarALink'

const pages: string[] = ["one", "two", "three"];

const NavBar = () => {
  const router = useRouter();
  const pathName = router.pathname.split("/");
  const url = pathName[2].toString();

  return (
    <AppBar position="static">
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Box sx={{ flexGrow: 1, display: "flex", flexWrap: "wrap" }}>
            {pages.map((page, i) => (
              <div key={i}><NavBarALink url={url} pageName={page} index={i}/></div>
            ))}
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default NavBar;
