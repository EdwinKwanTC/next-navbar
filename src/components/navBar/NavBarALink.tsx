import Link from "next/link";
import { styled } from "@mui/system";

const StyledALink = styled("a")({
  color: "darkslategray",
  backgroundColor: "aliceblue",
  marginRight: "10px",
  padding: "2px",
  borderRadius: "5%",
});

type NavBarALinkProps = {
  url: string;
  pageName: string;
  index: number;
};

const NavBarALink = (props: NavBarALinkProps) => {
  const index = props.index;
  const url = props.url;
  const pageName = props.pageName;

  let isMatch = false;
  if (pageName === url) {
    isMatch = true;
  }

  return (
    <>
      {isMatch ? (
        <Link href={`/some/${pageName}`}>
          <StyledALink style={{ marginRight: "10px" }}>{pageName}</StyledALink>
        </Link>
      ) : (
        <Link href={`/some/${pageName}`}>
          <a style={{ marginRight: "10px" }}>{pageName}</a>
        </Link>
      )}
    </>
  );
};

export default NavBarALink;
